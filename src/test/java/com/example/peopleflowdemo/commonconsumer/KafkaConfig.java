package com.example.peopleflowdemo.commonconsumer;

import com.example.peopleflowdemo.commonlib.kafka.dto.ChangeEmployeeStateKafkaDto;
import com.example.peopleflowdemo.commonlib.kafka.dto.EmployeeKafkaDto;
import com.example.peopleflowdemo.commonlib.kafka.dto.ResultKafkaDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;

/**
 * creates kafka template for consumer integration testing
 */
@Configuration
class KafkaConfig {

    @Value("${kafka-topics.employee-add-result.name}")
    private String addResultTopic;

    @Value("${groups.default-employee-add-result}")
    private String employeeAddResultGroup;

    @Bean
    public ReplyingKafkaTemplate<String, EmployeeKafkaDto, ResultKafkaDto> replyingKafkaTemplate(ProducerFactory<String, EmployeeKafkaDto> pf,
                                                                                                 ConcurrentKafkaListenerContainerFactory<String, ResultKafkaDto> factory) {
        ConcurrentMessageListenerContainer<String, ResultKafkaDto> replyContainer = factory.createContainer(addResultTopic);
        replyContainer.getContainerProperties().setGroupId(employeeAddResultGroup);
        return new ReplyingKafkaTemplate<>(pf, replyContainer);
    }

    @Bean
    public KafkaTemplate<String, ResultKafkaDto> replyTemplate(ProducerFactory<String, ResultKafkaDto> pf,
                                                               ConcurrentKafkaListenerContainerFactory<String, ResultKafkaDto> factory) {
        KafkaTemplate<String, ResultKafkaDto> kafkaTemplate = new KafkaTemplate<>(pf);
        factory.getContainerProperties().setMissingTopicsFatal(false);
        factory.setReplyTemplate(kafkaTemplate);
        return kafkaTemplate;
    }

    @Bean
    public KafkaTemplate<String, ChangeEmployeeStateKafkaDto> changeStateTemplate(ProducerFactory<String, ChangeEmployeeStateKafkaDto> pf) {
        return new KafkaTemplate<>(pf);
    }

}
