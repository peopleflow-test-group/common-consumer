package com.example.peopleflowdemo.commonconsumer;

import com.example.peopleflowdemo.commonconsumer.exception.EmployeeNotFoundEx;
import com.example.peopleflowdemo.commonconsumer.statemachine.persist.Persist;
import com.example.peopleflowdemo.commonlib.jsonb.ContractJsonb;
import com.example.peopleflowdemo.commonlib.kafka.dto.ChangeEmployeeStateKafkaDto;
import com.example.peopleflowdemo.commonlib.kafka.dto.EmployeeKafkaDto;
import com.example.peopleflowdemo.commonlib.kafka.dto.ResultKafkaDto;
import com.example.peopleflowdemo.commonlib.repository.EmployeeRepository;
import com.example.peopleflowdemo.commonlib.repository.entity.EmployeeEntity;
import com.example.peopleflowdemo.commonlib.statemachine.event.StateEvent;
import com.example.peopleflowdemo.commonlib.statemachine.state.EmployeeStateEnum;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.concurrent.ExecutionException;

@Import(KafkaConfig.class)
@SpringBootTest(args = {"--spring.config.name=bootstrap,common-consumer,common-config"})
@ActiveProfiles("test")
@AutoConfigureMockMvc
@Testcontainers
@EmbeddedKafka(
        partitions = 1,
        brokerProperties = {"listeners=PLAINTEXT://localhost:9092", "port=9092"}
)
@ContextConfiguration(initializers = MyPostgreSQLContainerInitializer.class)
class CommonConsumerApplicationTests {


    @Value("${kafka-topics.employee-add.name}")
    private String addTopic;

    @Value("${kafka-topics.employee-state.name}")
    private String stateTopic;

    @Autowired
    private EmployeeRepository repository;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private ReplyingKafkaTemplate<String, EmployeeKafkaDto, ResultKafkaDto> replyingKafkaTemplate;
    @Autowired
    private KafkaTemplate<String, ChangeEmployeeStateKafkaDto> changeStateTemplate;
    @Autowired
    private Persist persist;


    @BeforeEach
    void tearDown() {
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "employees");
    }


    @Test
    public void employeeKafkaDtoWithRepeatablePhone_test() throws ExecutionException, InterruptedException {

        EmployeeKafkaDto kafkaDto = createEmployeeKafkaDto();

        ResultKafkaDto resultKafkaDto = replyingKafkaTemplate.sendAndReceive(new ProducerRecord<>(addTopic, kafkaDto)).get().value();

        Assertions.assertEquals(
                resultKafkaDto.getId(),
                repository.findByMobilePhone(kafkaDto.getMobilePhone()).orElseThrow().getEmployeeId());
        Assertions.assertNull(resultKafkaDto.getResponseCode());

        ResultKafkaDto repeatResultKafkaDto = replyingKafkaTemplate.sendAndReceive(new ProducerRecord<>(addTopic, kafkaDto)).get().value();

        Assertions.assertEquals(repeatResultKafkaDto.getResponseCode(), "EMPMAN_4");
        Assertions.assertNull(repeatResultKafkaDto.getId());
    }


    @Test
    public void employeeStateTransition_test() throws InterruptedException {
        EmployeeKafkaDto kafkaDto = createEmployeeKafkaDto();
        replyingKafkaTemplate.sendAndReceive(new ProducerRecord<>(addTopic, createEmployeeKafkaDto()));
        Thread.sleep(1000);
        EmployeeEntity entity = repository.findByMobilePhone(kafkaDto.getMobilePhone()).orElseThrow();
        Assertions.assertEquals(EmployeeStateEnum.ADDED, entity.getState());

        ChangeEmployeeStateKafkaDto changeEmployeeStateKafkaDto = new ChangeEmployeeStateKafkaDto(entity.getEmployeeId(), StateEvent.CHECK);
        changeStateTemplate.send(stateTopic, changeEmployeeStateKafkaDto);
        Thread.sleep(1000);
        EmployeeEntity entityAfterUpdate = repository.findById(changeEmployeeStateKafkaDto.getEmployeeId()).orElseThrow();
        Assertions.assertEquals(EmployeeStateEnum.IN_CHECK, entityAfterUpdate.getState());
        System.out.println();
    }


    @Test
    public void notFoundException_test() {
        ChangeEmployeeStateKafkaDto kafkaDto = new ChangeEmployeeStateKafkaDto(1L, StateEvent.CHECK);
        Assertions.assertThrows(EmployeeNotFoundEx.class, () -> {
            persist.change(kafkaDto.getEmployeeId(), kafkaDto.getEvent());
        });


    }


    private EmployeeKafkaDto createEmployeeKafkaDto() {
        return new EmployeeKafkaDto(
                "pssHash",
                "John",
                "Doe",
                21,
                "555",
                "abc@gmail.com",
                EmployeeStateEnum.ADDED,
                new ContractJsonb(
                        5,
                        "programming/testing",
                        "some agreements"));
    }


}
