package com.example.peopleflowdemo.commonconsumer.statemachine;


import com.example.peopleflowdemo.commonconsumer.config.StateMachineConfig;
import com.example.peopleflowdemo.commonlib.statemachine.event.StateEvent;
import com.example.peopleflowdemo.commonlib.statemachine.state.EmployeeStateEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.test.StateMachineTestPlan;
import org.springframework.statemachine.test.StateMachineTestPlanBuilder;
import org.springframework.test.annotation.DirtiesContext;


@SpringBootTest(
        classes = {StateMachineConfig.class},
        args = {"--spring.config.name=bootstrap,common-consumer,common-config"})
public class StateMachineTests {


    @Autowired
    private StateMachine<EmployeeStateEnum, StateEvent> stateMachine;


    @DirtiesContext
    @Test
    public void eventsOrder_test() throws Exception {

        StateMachineTestPlan<EmployeeStateEnum, StateEvent> plan =
                StateMachineTestPlanBuilder.<EmployeeStateEnum, StateEvent>builder()
                        .defaultAwaitTime(2)
                        .stateMachine(stateMachine)
                        .step()
                        .expectStates(EmployeeStateEnum.ADDED)
                        .expectStateChanged(1)
                        .and()
                        .step()
                        .sendEvent(StateEvent.CHECK)
                        .expectState(EmployeeStateEnum.IN_CHECK)
                        .expectStateChanged(1)
                        .and()
                        .step()
                        .sendEvent(StateEvent.APPROVE)
                        .expectState(EmployeeStateEnum.APPROVED)
                        .expectStateChanged(1)
                        .and()
                        .step()
                        .sendEvent(StateEvent.ACTIVATE)
                        .expectState(EmployeeStateEnum.ACTIVE)
                        .expectStateChanged(1)
                        .and()
                        .build();
        plan.test();
    }

    @DirtiesContext
    @Test
    public void wrongEventOrder_test() throws Exception {

        StateMachineTestPlan<EmployeeStateEnum, StateEvent> plan =
                StateMachineTestPlanBuilder.<EmployeeStateEnum, StateEvent>builder()
                        .defaultAwaitTime(2)
                        .stateMachine(stateMachine)
                        .step()
                        .expectStates(EmployeeStateEnum.ADDED)
                        .expectStateChanged(1)
                        .and()
                        .step()
                        .sendEvent(StateEvent.ACTIVATE)
                        .expectState(EmployeeStateEnum.ADDED)
                        .expectStateChanged(0)
                        .and()
                        .step()
                        .sendEvent(StateEvent.APPROVE)
                        .expectState(EmployeeStateEnum.ADDED)
                        .expectStateChanged(0)
                        .and()
                        .step()
                        .sendEvent(StateEvent.CHECK)
                        .expectState(EmployeeStateEnum.IN_CHECK)
                        .expectStateChanged(1)
                        .and()
                        .build();
        plan.test();
    }

}
