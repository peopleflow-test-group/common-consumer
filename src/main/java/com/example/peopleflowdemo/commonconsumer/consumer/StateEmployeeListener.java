package com.example.peopleflowdemo.commonconsumer.consumer;

import com.example.peopleflowdemo.commonconsumer.exception.EmployeeNotFoundEx;
import com.example.peopleflowdemo.commonconsumer.statemachine.persist.Persist;
import com.example.peopleflowdemo.commonlib.kafka.dto.ChangeEmployeeStateKafkaDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.adapter.ConsumerRecordMetadata;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * Employee state changing functionality
 */
@AllArgsConstructor
@Slf4j
@ConditionalOnProperty(value = "bean.enable.StateEmployeeListener", havingValue = "true")
@Component
public class StateEmployeeListener {


    private final Persist persist;

    /**
     * performs kafka messages polls
     *
     * @param data kafka message with state update info
     * @param ack  manual ack/nack object
     * @param meta topic/offset etc attrs
     */
    @KafkaListener(topics = "${kafka-topics.employee-state.name}", groupId = "${groups.default-employee-state}")
    public void listen(@Payload ChangeEmployeeStateKafkaDto data, Acknowledgment ack, ConsumerRecordMetadata meta) {

        log.info("===== CONSUMER POLL =====\n\ntopic: {}\noffset: {}\nbody: {}\n", meta.topic(), meta.offset(), data);

        try {
            persist.change(data.getEmployeeId(), data.getEvent());
        } catch (EmployeeNotFoundEx ex) {
            log.error(ex.toString(), ex);
            log.error("Inconsistent state");
            ack.nack(10000);
        }

        ack.acknowledge();
    }


}
