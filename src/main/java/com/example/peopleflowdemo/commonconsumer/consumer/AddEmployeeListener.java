package com.example.peopleflowdemo.commonconsumer.consumer;

import com.example.peopleflowdemo.commonconsumer.exception.RepeatablePhoneEx;
import com.example.peopleflowdemo.commonconsumer.service.IReadWriteEmployeeService;
import com.example.peopleflowdemo.commonlib.kafka.dto.EmployeeKafkaDto;
import com.example.peopleflowdemo.commonlib.kafka.dto.ResultKafkaDto;
import com.example.peopleflowdemo.commonlib.mapping.IEmployeeMapper;
import com.example.peopleflowdemo.commonlib.repository.entity.EmployeeEntity;
import com.example.peopleflowdemo.commonlib.service.helper.EmployeeMappingHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.adapter.ConsumerRecordMetadata;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

/**
 * Replying listener is responsible for employee creation. Returns generated employee id or exception code.
 * Handles violations of the mobile phone unique constraint.
 */
@RequiredArgsConstructor
@Slf4j
@ConditionalOnProperty(value = "bean.enable.AddEmployeeListener", havingValue = "true")
@Service
public class AddEmployeeListener {


    private final IReadWriteEmployeeService readWriteEmployeeService;
    private final IEmployeeMapper mapper;


    /**
     * provides kafka consumer functionality
     *
     * @param data       input body
     * @param ack        manual ack/nack object
     * @param meta       topic/offset etc attrs
     * @param replyTopic is needed to return response
     * @return carries generated attrs
     */
    @SendTo
    @KafkaListener(topics = "${kafka-topics.employee-add.name}", groupId = "${groups.default-employee-add}")
    public ResultKafkaDto listen(@Payload EmployeeKafkaDto data,
                                 Acknowledgment ack,
                                 ConsumerRecordMetadata meta,
                                 @Header(KafkaHeaders.REPLY_TOPIC) String replyTopic) {
        log.info("===== CONSUMER POLL =====\n\ntopic: {}\noffset: {}\nbody: {}\n", meta.topic(), meta.offset(), data);

        try {
            readWriteEmployeeService.getByMobilePhoneThenThrow(data.getMobilePhone(), new RepeatablePhoneEx(data.getMobilePhone()));
        } catch (RepeatablePhoneEx ex) {
            log.error(ex.toString(), ex);
            return acknowledgeAndSend(replyTopic, ack, ResultKafkaDto.createExceptional("EMPMAN_4"));
        }
        EmployeeEntity entity = readWriteEmployeeService.add(mapper.toEntity(data, EmployeeMappingHelper.createEntityHelper(ZonedDateTime.now())));
        return acknowledgeAndSend(replyTopic, ack, ResultKafkaDto.createSuccess(entity.getEmployeeId(), entity.getOpenDate()));
    }


    /**
     * performs manual acknowledge, replying message logging and returning
     *
     * @param replyTopic     topic for replying result
     * @param ack            kafka manual acknowledge object
     * @param resultKafkaDto replying message body
     * @return resultKafkaDto
     */
    private ResultKafkaDto acknowledgeAndSend(String replyTopic, Acknowledgment ack, ResultKafkaDto resultKafkaDto) {
        ack.acknowledge();
        log.info("===== CONSUMER REPLY =====\n\ntopic: {}\nbody: {}\n", replyTopic, resultKafkaDto);
        return resultKafkaDto;
    }


}
