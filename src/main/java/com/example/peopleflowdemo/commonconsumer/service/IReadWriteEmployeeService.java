package com.example.peopleflowdemo.commonconsumer.service;

import com.example.peopleflowdemo.commonlib.repository.entity.EmployeeEntity;
import com.example.peopleflowdemo.commonlib.statemachine.state.EmployeeStateEnum;
import com.example.peopleflowdemo.commonlib.service.IReadEmployeeService;
import org.springframework.transaction.annotation.Transactional;

/**
 * provides employee read/write base functionality
 */
public interface IReadWriteEmployeeService extends IReadEmployeeService {


    /**
     * makes an insert
     *
     * @param entity should be saved in the repository
     * @return includes generated id
     */
    EmployeeEntity add(EmployeeEntity entity);


    /**
     * native query employee state update
     *
     * @param state      new state
     * @param employeeId target entity id
     */
    void updateState(EmployeeStateEnum state, Long employeeId);
}
