package com.example.peopleflowdemo.commonconsumer.service.impl;

import com.example.peopleflowdemo.commonconsumer.service.IReadWriteEmployeeService;
import com.example.peopleflowdemo.commonlib.repository.EmployeeRepository;
import com.example.peopleflowdemo.commonlib.repository.entity.EmployeeEntity;
import com.example.peopleflowdemo.commonlib.service.ReadEmployeeService;
import com.example.peopleflowdemo.commonlib.statemachine.state.EmployeeStateEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ReadWriteEmployeeService extends ReadEmployeeService implements IReadWriteEmployeeService {


    @Autowired
    public ReadWriteEmployeeService(EmployeeRepository repository) {
        super(repository);
    }


    @Override
    @Transactional
    public EmployeeEntity add(EmployeeEntity entity) {
        entity = repository.save(entity);
        return entity;
    }


    @Override
    @Transactional
    public void updateState(EmployeeStateEnum state, Long employeeId) {
        repository.nativeStateUpdate(state.name(), employeeId);
    }


}
