package com.example.peopleflowdemo.commonconsumer.config;


import com.example.peopleflowdemo.commonconsumer.statemachine.action.DefaultAction;
import com.example.peopleflowdemo.commonlib.statemachine.event.StateEvent;
import com.example.peopleflowdemo.commonlib.statemachine.state.EmployeeStateEnum;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import static com.example.peopleflowdemo.commonlib.statemachine.event.StateEvent.*;
import static com.example.peopleflowdemo.commonlib.statemachine.state.EmployeeStateEnum.*;


@Configuration
@EnableStateMachine
public class StateMachineConfig extends EnumStateMachineConfigurerAdapter<EmployeeStateEnum, StateEvent> {


    @Override
    public void configure(StateMachineStateConfigurer<EmployeeStateEnum, StateEvent> states) throws Exception {
        states
                .withStates()
                .initial(ADDED)
                .state(IN_CHECK)
                .state(APPROVED)
                .state(ACTIVE);
    }


    @Override
    public void configure(StateMachineTransitionConfigurer<EmployeeStateEnum, StateEvent> transitions) throws Exception {
        transitions
                .withExternal()
                .source(ADDED).target(IN_CHECK)
                .event(CHECK)
                .action(defaultAction())

                .and()
                .withExternal()
                .source(IN_CHECK).target(APPROVED)
                .event(APPROVE)
                .action(defaultAction())

                .and()
                .withExternal()
                .source(APPROVED).target(ACTIVE)
                .event(ACTIVATE)
                .action(defaultAction());
    }


    @Bean
    public Action<EmployeeStateEnum, StateEvent> defaultAction() {
        return new DefaultAction();
    }


}
