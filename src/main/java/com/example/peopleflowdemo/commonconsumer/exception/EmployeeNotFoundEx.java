package com.example.peopleflowdemo.commonconsumer.exception;

import java.text.MessageFormat;

/**
 * is thrown in inconsistent cases when employee is not found in DB
 */
public class EmployeeNotFoundEx extends RuntimeException {

    public EmployeeNotFoundEx(String message) {
        super(message);
    }

    public EmployeeNotFoundEx(Long employeeId) {
        super(MessageFormat
                .format("Employee id={0} not found", employeeId));
    }
}
