package com.example.peopleflowdemo.commonconsumer.exception;

import java.text.MessageFormat;

/**
 * is thrown in mobile phone unique constraint violation cases
 */
public class RepeatablePhoneEx extends RuntimeException {

    public RepeatablePhoneEx(String phoneNumber) {
        super(MessageFormat
                .format("Phone number {0} already exists", phoneNumber));
    }
}
