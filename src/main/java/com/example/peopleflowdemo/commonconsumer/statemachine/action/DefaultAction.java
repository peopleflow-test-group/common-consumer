package com.example.peopleflowdemo.commonconsumer.statemachine.action;

import com.example.peopleflowdemo.commonlib.statemachine.state.EmployeeStateEnum;
import com.example.peopleflowdemo.commonlib.statemachine.event.StateEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

@Slf4j
public class DefaultAction implements Action<EmployeeStateEnum, StateEvent> {
    @Override
    public void execute(final StateContext<EmployeeStateEnum, StateEvent> context) {

        log.info(
                "StateMachine transition in action:\nemployeeId: {}\nevent: {}\nsource: {}\ntarget: {}",
                context.getMessage().getHeaders().get("employeeId"),
                context.getEvent(),
                context.getSource().getId(),
                context.getTarget().getId());
    }
}