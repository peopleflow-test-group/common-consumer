package com.example.peopleflowdemo.commonconsumer.statemachine.persist;

import com.example.peopleflowdemo.commonconsumer.service.IReadWriteEmployeeService;
import com.example.peopleflowdemo.commonlib.statemachine.event.StateEvent;
import com.example.peopleflowdemo.commonlib.statemachine.state.EmployeeStateEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.recipes.persist.GenericPersistStateMachineHandler;

/**
 * Persist object bean initialization config
 */
@Configuration
public class PersistHandlerConfig {


    private final StateMachine<EmployeeStateEnum, StateEvent> stateMachine;
    private final IReadWriteEmployeeService readWriteEmployeeService;

    @Autowired
    public PersistHandlerConfig(StateMachine<EmployeeStateEnum, StateEvent> stateMachine, IReadWriteEmployeeService readWriteEmployeeService) {
        this.stateMachine = stateMachine;
        this.readWriteEmployeeService = readWriteEmployeeService;
    }


    @Bean
    public Persist persist() {
        return new Persist(genericPersistStateMachineHandler(), readWriteEmployeeService);
    }


    @Bean
    public GenericPersistStateMachineHandler<EmployeeStateEnum, StateEvent> genericPersistStateMachineHandler() {
        return new GenericPersistStateMachineHandler<>(stateMachine);
    }
}
