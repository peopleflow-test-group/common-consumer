package com.example.peopleflowdemo.commonconsumer.statemachine.persist;

import com.example.peopleflowdemo.commonconsumer.exception.EmployeeNotFoundEx;
import com.example.peopleflowdemo.commonconsumer.service.IReadWriteEmployeeService;
import com.example.peopleflowdemo.commonlib.repository.entity.EmployeeEntity;
import com.example.peopleflowdemo.commonlib.statemachine.state.EmployeeStateEnum;
import com.example.peopleflowdemo.commonlib.statemachine.event.StateEvent;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.recipes.persist.AbstractPersistStateMachineHandler.GenericPersistStateChangeListener;
import org.springframework.statemachine.recipes.persist.GenericPersistStateMachineHandler;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;

/**
 * provides DB employee state updates according to possible statemachine transitions
 */
public class Persist {


    private final GenericPersistStateMachineHandler<EmployeeStateEnum, StateEvent> handler;
    private final IReadWriteEmployeeService readWriteEmployeeService;

    public Persist(GenericPersistStateMachineHandler<EmployeeStateEnum, StateEvent> handler, IReadWriteEmployeeService readWriteEmployeeService) {
        this.handler = handler;
        this.handler.addPersistStateChangeListener(new LocalPersistStateChangeListener());
        this.readWriteEmployeeService = readWriteEmployeeService;
    }


    /**
     * sends an event to the state machine for particular employee
     *
     * @param employeeId target employee to be updated
     * @param event state machine event for transition
     */
    public void change(Long employeeId, StateEvent event) {
        EmployeeEntity entity = readWriteEmployeeService.getByIdOrElseThrow(
                employeeId,
                () -> new EmployeeNotFoundEx(employeeId));

        handler.handleEventWithStateReactively(
                MessageBuilder
                        .withPayload(event)
                        .setHeader("employeeId", employeeId)
                        .build(),
                entity.getState())
                .subscribe();
    }


    /**
     * persistence logic after state machine transition
     */
    private class LocalPersistStateChangeListener implements GenericPersistStateChangeListener<EmployeeStateEnum, StateEvent> {

        /**
         * updates employee state in DB according to the state machine transition
         *
         * @param state
         * @param message
         * @param transition
         * @param stateMachine
         */
        @Override
        public void onPersist(State<EmployeeStateEnum, StateEvent> state,
                              Message<StateEvent> message,
                              Transition<EmployeeStateEnum, StateEvent> transition,
                              StateMachine<EmployeeStateEnum, StateEvent> stateMachine) {

            if (message != null && message.getHeaders().containsKey("employeeId")) {
                Long employeeId = message.getHeaders().get("employeeId", Long.class);
                readWriteEmployeeService.updateState(state.getId(), employeeId);
            }
        }
    }
}
