# common-consumer

Spring Boot service for common consumer. Kafka communication service with read/write DB access.

## Build
```
mvn clean package  
docker build -t mishgun8ku/common-consumer:latest .  
docker push mishgun8ku/common-consumer:latest  
docker run -d --rm -p8081:8081 mishgun8ku/common-consumer:latest
```